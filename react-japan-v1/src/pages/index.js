import React from "react"
import Email from "../components/Email"
import Hero from "../components/Hero"
import Layout from "../components/layout"
import Peoples from "../components/Peoples"
import SEO from "../components/seo"
import State from "../components/State"
import Trips from "../components/Trips"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Hero/>
    <Trips heading="Alors tu viens ou pas ?"/>
    <Peoples/>
    <State/>
    <Email/>
  </Layout>
)

export default IndexPage
