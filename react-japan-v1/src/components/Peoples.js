import React from 'react'
import styled from "styled-components"
import Img from 'gatsby-image'
import {IoMdCheckmarkCircleOutline} from 'react-icons/io'
import {FaRegLightbulb} from "react-icons/fa"
import { graphql, useStaticQuery } from 'gatsby'

const Peoples = () => {
    const data = useStaticQuery(graphql`
    query  {
        allFile(filter: {ext: {regex: "/jpg/"}, 
          name: {in: ["people1","people2"]}}) {
          edges {
            node {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
      
      
    
    `)
    return (
        <PeopleContainer>
            <TopLine>
                Avis
            </TopLine>
            <Description>
                Les personnes pense
            </Description>
            <ContainerWrapper>
                <ColumnOne>
                <People>
                    <IoMdCheckmarkCircleOutline/>
                    <h3>Guts mukuta</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa ab quia blanditiis cupiditate odit sint beatae saepe distinctio? Totam repellendus aut aliquid ipsum eveniet ea, et accusamus velit excepturi dicta.</p>
                </People>
                <People>
                    <FaRegLightbulb css={
                        `color: #3fffa8;
                    font-size:2rem;
                    margin-bottom:1rem` }/>
                    <h3>Julie Kin</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa ab quia blanditiis cupiditate odit sint beatae saepe distinctio? Totam repellendus aut aliquid ipsum eveniet ea, et accusamus velit excepturi dicta.</p>
                </People>
                </ColumnOne>
                <ColumnTwo>
                {data.allFile.edges.map((image,key) => (
                    <Images key={key} fluid={image.node.childImageSharp.fluid}/>

                ))}
                </ColumnTwo>
            </ContainerWrapper>
        </PeopleContainer>
    )
}

export default Peoples



const PeopleContainer = styled.div`
width: 100%;
background:#04021a;
color:white;
padding: 5rem calc((100vw - 1300px)/2);
height:100%;

`

const TopLine = styled.p`
color:#077bf1;
font-size:1rem;
padding-left:2rem;
margin-bottom:0.75rem;
`

const Description = styled.p`
text-align:start;
padding-left:2rem;
margin-bottom: 4rem;
font-size: clamp(1.5rem,5vw,2rem);
font-weight: bold;

`

const ContainerWrapper = styled.div`
display:grid;
grid-template-columns: 1fr 1fr;
padding:0 2rem;

@media screen and (max-width:768px){
    grid-template-columns: 1fr;
}
`

const ColumnOne = styled.div`
display: grid;
grid-template-rows: 1fr 1fr;
`
const People = styled.div`
padding-top:1rem;
padding-right: 2rem;

h3{
    margin-bottom:1rem;
    font-size:1.5rem;
    font-style: italic;
}

p{
    color:white;
}
`

const ColumnTwo = styled.div`
display:grid;
grid-template-columns: 1fr 1fr;
margin-top:2rem;
grid-gap: 10px;

@media screen and (max-width: 500px){
    grid-template-columns: 1fr;
}
`

const Images = styled(Img)`
border-radius: 10px;
height:100%;
`