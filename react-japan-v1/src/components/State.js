import React from 'react'
import styled from "styled-components"
import {GiEarthAsiaOceania} from "react-icons/gi"
import {MdAirplanemodeActive,MdTimer} from "react-icons/md"
import {FaMoneyCheck} from "react-icons/fa"


const StateData = [

    {
        icon:(<GiEarthAsiaOceania/>),
        title: "alors ces parti",
        desc: "Dans que ta pas encore connu"
    },
    {
        icon:(<MdAirplanemodeActive/>),
        title: "alors ces parti",
        desc: "Dans que ta pas encore connu"
    }, {
        icon:(<MdTimer/>),
        title: "alors ces parti",
        desc: "Dans que ta pas encore connu"
    }, {
        icon:(<FaMoneyCheck/>),
        title: "alors ces parti",
        desc: "Dans que ta pas encore connu"
    },
]

const State = () => {
    return (
        <StateContainer>
            <Heading>Choisis</Heading>
    <Wrapper>{StateData.map((item,index) => {
        return(
        <StatsBox key={index}>
           <Icon> {item.icon}</Icon>
        <Title key={index}>{item.title}</Title>
        <Description key={index}>{item.desc}</Description>
        </StatsBox>
        )
    })}</Wrapper>
        </StateContainer>
    )
}

export default State

const StateContainer = styled.div`
width: 100%; 
background:#04021a;
color:white;
display:flex;
flex-direction:column;
justify-content:center;
padding: 4rem calc((100vw - 1300px)/2);
`

const Heading = styled.div`
text-align: start;
font-size: clamp(1.5rem, 5vw, 2rem);
margin-bottom:3rem;
padding: 0 2rem; 

`

const Wrapper = styled.div`
display: grid;
grid-template-columns: repeat(4, 1fr);
grid-gap: 10px;

@media screen and (max-width:780px){
    grid-template-columns: 1fr;
}
@media screen and (max-width:500px){
    grid-template-columns: 1fr;
}
`

const StatsBox = styled.div`
height:100%;
width:100%;
padding: 2rem;
`
const Icon= styled.div`
font-size: 3rem;
margin-bottom: 1rem;
`
const Title = styled.div`
font-size: clamp(1rem,2.5vw,1.5rem);
margin-bottom: 0.5rem;
`
const Description = styled.div`

`