import React from 'react'
import styled from "styled-components"
import {Link} from 'gatsby'

const Footer = () => {
    return (
        <FooterContainer>
            <FooterLinksWrapper>
                <FooterDes>
                    <h1>Night</h1>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                </FooterDes>
                <FooterLinksItems>
                    <FooterLinkTitle>Contact us</FooterLinkTitle>
                    <FooterLink to="/about">Contact</FooterLink>
                    <FooterLink to="/">Support</FooterLink>
                    <FooterLink to="/">Destination</FooterLink>
                    <FooterLink to="/">Sponsor</FooterLink>
                </FooterLinksItems>
            </FooterLinksWrapper>
            <FooterLinksWrapper>
            <FooterLinksItems>
                    <FooterLinkTitle>Video</FooterLinkTitle>
                    <FooterLink to="/about">Submit Video</FooterLink>
                    <FooterLink to="/">Ambassadeur</FooterLink>
                    <FooterLink to="/">Agence</FooterLink>
                    <FooterLink to="/">Influenceur(euse)</FooterLink>
                </FooterLinksItems>
                <FooterLinksItems>
                    <FooterLinkTitle>Reseau</FooterLinkTitle>
                    <FooterLink to="/">Instagram</FooterLink>
                    <FooterLink to="/">Facebook</FooterLink>
                    <FooterLink to="/">Twitter</FooterLink>
                    <FooterLink to="/">Youtube</FooterLink>
                </FooterLinksItems>
            </FooterLinksWrapper>
        </FooterContainer>
    )
}

export default Footer


const FooterContainer = styled.div`
padding: 5rem calc((100vw - 1100px)/2);
display:grid;
grid-template-columns: repeat(2,1fr);
color: white;
background:#04021a;
`

const FooterDes = styled.div`
padding: 0 2rem;

h1{
    margin-bottom: 3rem;
    color:#f26a2e;
}

@media screen and(max-width:400px){
    padding: 1rem;
}
`

const FooterLinksWrapper = styled.div`
display: grid;
grid-template-columns: repeat(2,1fr);

@media scren and (max-width:820px){
    grid-template-columns: 1fr;
}
`

const FooterLinksItems = styled.div`
display: flex;
flex-direction: column;
align-items: flex-end;
padding: 1rem 2rem;

@media screen and (max-width:400px){
    padding: 1rem;
}
`

const FooterLinkTitle = styled.h2`
font-size: 14px;
margin-bottom: 16px;

`

const FooterLink = styled(Link)`
text-decoration: none;
margin-bottom: 0.5;
font-size:14px;
color:white;

&:hover{
    color: #f26a2e;
    transition: 0.3s ease-out;
}
`


